package com.adapp.adapp_pos_models.utils;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class IdGenerationUtilityTest {

	@Test
	@Disabled
	public void generateId() {
		List<String> generatedIds = new ArrayList<>();
		for (int x = 0; x < 675999324; x++) {
			String generatedId = IdGenerationUtility.generateId((id) -> generatedIds.contains(id));
			Assertions.assertFalse(generatedIds.contains(generatedId));
			generatedIds.add(generatedId);
		}
	}
}