package com.adapp.adapp_pos_models.enums;

public enum DocType {
	PDF,
	XLS,
	DOCX,
	CSV
}
