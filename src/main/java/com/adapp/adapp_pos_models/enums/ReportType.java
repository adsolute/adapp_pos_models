package com.adapp.adapp_pos_models.enums;

public enum ReportType {
	INVOICE,
	DAILY_SALES_REPORT,
	INVOICE_RECEIPT;
}
