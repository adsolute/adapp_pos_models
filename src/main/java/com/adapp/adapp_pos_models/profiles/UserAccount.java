package com.adapp.adapp_pos_models.profiles;

import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.PersonalDetails;
import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.security.models.Account;
import com.adapp.security.models.GrantedPermission;
import com.adapp.security.models.GrantedRole;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document("accounts")
public class UserAccount extends Account {

	private PersonalDetails personalDetails;
	private ContactInformation contactInformation;
	private List<GrantedRole> grantedRoles = new ArrayList<>();
	private Set<GrantedPermission<Resource>> grantedPermissions= new HashSet<>();

	public PersonalDetails getPersonalDetails() {
		return personalDetails;
	}

	public void setPersonalDetails(PersonalDetails personalDetails) {
		this.personalDetails = personalDetails;
	}

	public ContactInformation getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(ContactInformation contactInformation) {
		this.contactInformation = contactInformation;
	}

	@Override
	public List<GrantedRole> getGrantedRoles() {
		return grantedRoles;
	}

	public void setGrantedRoles(List<GrantedRole> grantedRoles) {
		this.grantedRoles = grantedRoles;
	}

	@Override
	public Set<GrantedPermission<Resource>> getGrantedPermissions() {
		return grantedPermissions;
	}

	public void setGrantedPermissions(Set<GrantedPermission<Resource>> grantedPermissions) {
		this.grantedPermissions = grantedPermissions;
	}
}
