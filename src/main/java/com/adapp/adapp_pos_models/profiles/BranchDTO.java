package com.adapp.adapp_pos_models.profiles;

import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;


public class BranchDTO {
	private String id;
	private String branchId;
	private String name;
	private String address;
	private String email;
	private String phone;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime createDate;

	public BranchDTO() {
	}

	public BranchDTO(String id, String branchId, String name, String address, String email, String phone, DateTime createDate) {
		this.id = id;
		this.branchId = branchId;
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public DateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}
	
}
