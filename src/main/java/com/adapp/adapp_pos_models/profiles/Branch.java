package com.adapp.adapp_pos_models.profiles;

import com.adapp.adapp_pos_models.Address;
import com.adapp.adapp_pos_models.ContactInformation;
import com.adapp.adapp_pos_models.invoices.DeliveryCharge;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Branch {
    @Id
    private String id;
    private String branchId;
    private String name;
    private Address address;
    private ContactInformation contactInformation;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime createDate;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime updateDate;
    private BranchConfiguration branchConfiguration;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public BranchConfiguration getBranchConfiguration() {
        return branchConfiguration;
    }

    public void setBranchConfiguration(BranchConfiguration branchConfiguration) {
        this.branchConfiguration = branchConfiguration;
    }

    public static class BranchConfiguration {
        private int numberOfTables;
        private boolean canViewUniversalProducts;
        private boolean hasOwnProducts;
        private BigDecimal serviceChargePercentage;
        private List<DeliveryCharge> deliveryCharges = new ArrayList<>();
        private BigDecimal discountPercentage;

        public BigDecimal getDiscountPercentage() {
            return discountPercentage;
        }

        public void setDiscountPercentage(BigDecimal discountPercentage) {
            this.discountPercentage = discountPercentage;
        }

        public List<DeliveryCharge> getDeliveryCharges() {
            return deliveryCharges;
        }

        public void setDeliveryCharges(List<DeliveryCharge> deliveryCharges) {
            this.deliveryCharges = deliveryCharges;
        }

        public int getNumberOfTables() {
            return numberOfTables;
        }

        public void setNumberOfTables(int numberOfTables) {
            this.numberOfTables = numberOfTables;
        }

        public boolean isCanViewUniversalProducts() {
            return canViewUniversalProducts;
        }

        public void setCanViewUniversalProducts(boolean canViewUniversalProducts) {
            this.canViewUniversalProducts = canViewUniversalProducts;
        }

        public boolean isHasOwnProducts() {
            return hasOwnProducts;
        }

        public void setHasOwnProducts(boolean hasOwnProducts) {
            this.hasOwnProducts = hasOwnProducts;
        }

        public BigDecimal getServiceChargePercentage() {
            return serviceChargePercentage;
        }

        public void setServiceChargePercentage(BigDecimal serviceChargePercentage) {
            this.serviceChargePercentage = serviceChargePercentage;
        }
    }
}
