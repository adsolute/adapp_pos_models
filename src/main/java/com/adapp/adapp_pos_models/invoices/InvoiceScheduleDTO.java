package com.adapp.adapp_pos_models.invoices;

import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;

public class InvoiceScheduleDTO {

	private String id;
	private String title;
	private String description;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime start;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime end;
	private String status;
	private String city;
	private boolean forSameDayDelivery;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DateTime getStart() {
		return start;
	}

	public void setStart(DateTime start) {
		this.start = start;
	}

	public DateTime getEnd() {
		return end;
	}

	public void setEnd(DateTime end) {
		this.end = end;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public boolean isForSameDayDelivery() {
		return forSameDayDelivery;
	}

	public void setForSameDayDelivery(boolean forSameDayDelivery) {
		this.forSameDayDelivery = forSameDayDelivery;
	}
}
