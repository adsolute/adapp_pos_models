package com.adapp.adapp_pos_models.invoices;

import com.adapp.adapp_pos_models.payments.PaymentDetails;
import com.adapp.adapp_pos_models.payments.PaymentDetails.PaymentStatus;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public class InvoiceDTO {
	private String id;
	private String invoiceId;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime dueDate;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime invoiceDate;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime updateDate;
	private String updatedBy;
	private String number;
	private String customer;
	private BigDecimal amountDue;
	private String status;
	private String transactionType;
	private String tableNumber;
	private String branchId;
	private PaymentStatus paymentStatus;
	private Boolean voided;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public DateTime getDueDate() {
		return dueDate;
	}

	public void setDueDate(DateTime dueDate) {
		this.dueDate = dueDate;
	}

	public DateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(DateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public DateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(DateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public BigDecimal getAmountDue() {
		return amountDue;
	}

	public void setAmountDue(BigDecimal amountDue) {
		this.amountDue = amountDue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTableNumber() {
		return tableNumber;
	}

	public void setTableNumber(String tableNumber) {
		this.tableNumber = tableNumber;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Boolean getVoided() {
		return voided;
	}

	public void setVoided(Boolean voided) {
		this.voided = voided;
	}
}
