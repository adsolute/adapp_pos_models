package com.adapp.adapp_pos_models.invoices;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "invoice_number_trackers")
public class InvoiceNumberTracker {

	private String branchId;
	private int currentNumber;

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public int getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber(int currentNumber) {
		this.currentNumber = currentNumber;
	}
}
