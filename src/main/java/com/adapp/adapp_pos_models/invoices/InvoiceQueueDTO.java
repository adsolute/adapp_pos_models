package com.adapp.adapp_pos_models.invoices;

import com.adapp.adapp_pos_models.invoices.Invoice.Item;

import java.util.List;

public class InvoiceQueueDTO {
	private String id;
	private String invoiceNumber;
	private String customerName;
	private List<Item> items;
	private boolean late;
	private String deliveryDateTimeString;
	private boolean forSameDayDelivery;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public boolean getLate() {
		return late;
	}

	public void setLate(boolean late) {
		this.late = late;
	}

	public String getDeliveryDateTimeString() {
		return deliveryDateTimeString;
	}

	public void setDeliveryDateTimeString(String deliveryDateTimeString) {
		this.deliveryDateTimeString = deliveryDateTimeString;
	}

	public boolean isForSameDayDelivery() {
		return forSameDayDelivery;
	}

	public void setForSameDayDelivery(boolean forSameDayDelivery) {
		this.forSameDayDelivery = forSameDayDelivery;
	}
}
