package com.adapp.adapp_pos_models.invoices;

import com.adapp.adapp_pos_models.IdentifiableByBranch;
import com.adapp.adapp_pos_models.PersonalDetails;
import com.adapp.adapp_pos_models.payments.PaymentDetails;
import com.adapp.adapp_pos_models.customers.Customer;
import com.adapp.adapp_pos_models.products.Product;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Document(collection = "invoices")
public class Invoice implements IdentifiableByBranch {

	@Id
	private String id;
	private String invoiceId;
	private String branchId;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime createDate;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime updateDate;
	private String updatedBy;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime invoiceDate;
	private DeliveryStatus deliveryStatus;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime dueDate;
	@JsonSerialize(using = UTC8Iso8601Serializer.class)
	@JsonDeserialize(using = UTC8Iso8601Deserializer.class)
	private DateTime deliveryDate;
	private String number;
	private Customer customer;
	private BigDecimal amountDue;
	private BigDecimal discount = BigDecimal.ZERO;
	private List<Item> items = new ArrayList<>();
	private List<Note> notes = new ArrayList<>();
	private TransactionType transactionType;
	private String tableNumber;
	private RiderDetails riderDetails;
	private PaymentDetails paymentDetails;
	private DeliveryCharge deliveryCharge;
	private boolean voided;
	private String remarks;
	private boolean withServiceCharge;
	private BigDecimal serviceCharge;
	private int numberOfPax;
	private boolean withDiscount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }

	public DateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(DateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public DateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(DateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public DateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

	public DateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(DateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(BigDecimal amountDue) {
        this.amountDue = amountDue;
    }

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getTableNumber() {
		return tableNumber;
	}

	public void setTableNumber(String tableNumber) {
		this.tableNumber = tableNumber;
	}

	public RiderDetails getRiderDetails() {
		return riderDetails;
	}

	public void setRiderDetails(RiderDetails riderDetails) {
		this.riderDetails = riderDetails;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public DeliveryCharge getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(DeliveryCharge deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public boolean isVoided() {
		return voided;
	}

	public void setVoided(boolean voided) {
		this.voided = voided;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isWithServiceCharge() {
		return withServiceCharge;
	}

	public void setWithServiceCharge(boolean withServiceCharge) {
		this.withServiceCharge = withServiceCharge;
	}

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public int getNumberOfPax() {
		return numberOfPax;
	}

	public void setNumberOfPax(int numberOfPax) {
		this.numberOfPax = numberOfPax;
	}

	public boolean isWithDiscount() {
		return withDiscount;
	}

	public void setWithDiscount(boolean withDiscount) {
		this.withDiscount = withDiscount;
	}

	public enum DeliveryStatus {
		PROCESSING("Processing"),
		FOR_DELIVERY("For Delivery"),
		DELIVERED("Delivered"),
		FOR_PICK_UP("For Pick Up"),
		DONE("Done");

		private final String status;

		DeliveryStatus(String status) {
			this.status = status;
		}

	    public String getStatus() {
		    return status;
	    }
    }

	public static class Item {
	    private int quantity;
	    private Product product;

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }
    }

    public static class Note {
        private String type;
        private String message;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public enum TransactionType {
		DINE_IN("Dine in"),
	    DELIVERY("Delivery"),
		TAKE_OUT("Take Out");

		private String type;

		public String getTransactionType() {
			return type;
		}

	    TransactionType(String transactionType) {
			this.type = transactionType;
	    }
    }

    public static class RiderDetails {
    	private PersonalDetails personalDetails;
    	private String accountId;

	    public PersonalDetails getPersonalDetails() {
		    return personalDetails;
	    }

	    public void setPersonalDetails(PersonalDetails personalDetails) {
		    this.personalDetails = personalDetails;
	    }

	    public String getAccountId() {
		    return accountId;
	    }

	    public void setAccountId(String accountId) {
		    this.accountId = accountId;
	    }
    }
}
