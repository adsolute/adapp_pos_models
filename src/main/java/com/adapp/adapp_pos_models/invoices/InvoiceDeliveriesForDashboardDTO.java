package com.adapp.adapp_pos_models.invoices;

public class InvoiceDeliveriesForDashboardDTO {
    private long deliveriesToday;
    private long sameDayDeliveries;
    private long deliveredToday;
    private long remainingDeliveries;

    public long getDeliveriesToday() {
        return deliveriesToday;
    }

    public void setDeliveriesToday(long deliveriesToday) {
        this.deliveriesToday = deliveriesToday;
    }

    public long getSameDayDeliveries() {
        return sameDayDeliveries;
    }

    public void setSameDayDeliveries(long sameDayDeliveries) {
        this.sameDayDeliveries = sameDayDeliveries;
    }

    public long getDeliveredToday() {
        return deliveredToday;
    }

    public void setDeliveredToday(long deliveredToday) {
        this.deliveredToday = deliveredToday;
    }

    public long getRemainingDeliveries() {
        return remainingDeliveries;
    }

    public void setRemainingDeliveries(long remainingDeliveries) {
        this.remainingDeliveries = remainingDeliveries;
    }
}
