package com.adapp.adapp_pos_models.utils;

import com.google.common.primitives.Booleans;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public class SecurityCheckUtility {

	private SecurityCheckUtility() {

	}

	public static void checkPermission(String onAccountId, String permission) {
		if(!SecurityUtils.getSubject()
				.isPermitted(String.format("%s:%s", onAccountId, permission))) {
			throw new UnauthorizedException(
					String.format("Unauthorized action on: %s, %s permission is required",
							onAccountId,
							permission));
		}
	}

	public static void checkPermission(String onAccountId, List<String> permissions) {
		List<String> onAccountPermissions = permissions.stream().map(permission -> String.format("%s:%s", onAccountId, permission)).collect(Collectors.toList());

		List<Boolean> isPermittedResults = Booleans.asList(SecurityUtils.getSubject().isPermitted(onAccountPermissions.toArray(new String[permissions.size()])));

		if(!isPermittedResults.contains(true)) {
			throw new UnauthorizedException(String.format("Unauthorized action on: %s, User does not have any of the ff permissions: %s",
					onAccountId,
					String.join("\n", permissions)));
		}
	}
}
