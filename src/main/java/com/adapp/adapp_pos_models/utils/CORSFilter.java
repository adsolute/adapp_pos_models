package com.adapp.adapp_pos_models.utils;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.List;

@Provider
public class CORSFilter implements ContainerResponseFilter {

	@Override
	public void filter(final ContainerRequestContext requestContext, final ContainerResponseContext containerResponseContext )
			throws IOException {

		List<String> origins = requestContext.getHeaders().get( "Origin" );

		if ( origins != null && !origins.isEmpty() ) {
			// TODO: only allow pre-defined list of client url
			for ( String origin : origins ) {
				containerResponseContext.getHeaders().add( "Access-Control-Allow-Origin", origin );
			}
		}

		containerResponseContext.getHeaders().add( "Access-Control-Allow-Headers",
				"origin, withCredentials, content-type, accept, authorization, Access-Control-Allow-Origin, Set-Cookie, Access-Control-Allow-Credentials, Access-Control-Allow-Headers" );
		containerResponseContext.getHeaders().add( "Access-Control-Allow-Credentials", true );
		containerResponseContext.getHeaders().add( "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD" );
		containerResponseContext.getHeaders().add( "Access-Control-Max-Age", "1209600" );
		containerResponseContext.getHeaders().add( "Access-Control-Expose-Headers", "Location" );

	}
}
