package com.adapp.adapp_pos_models.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
public class JacksonMapperContextResolver implements ContextResolver<ObjectMapper> {

	private final ObjectMapper objectMapper;

	public JacksonMapperContextResolver() {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
	}

	@Override
	public ObjectMapper getContext(Class<?> aClass) {
		return objectMapper;
	}
}
