package com.adapp.adapp_pos_models.utils;

import com.mifmif.common.regex.Generex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Predicate;

public class IdGenerationUtility {

	private static final String REGEX = "[A-Z]{2}\\d{6}";
	private static final int MAX_COMBINATON = 675999324;
	private static final Logger LOGGER = LoggerFactory.getLogger(IdGenerationUtility.class);

	public static String generateId(Predicate<String> predicate) {
		int counter = 0;
		Generex generex = new Generex(REGEX);
		String generatedId = generex.random();
		if (predicate.test(generatedId)) {
			counter++;
			if (counter == MAX_COMBINATON) {
				LOGGER.info("Maximum combination of generated id reached!!!!");
			}
			LOGGER.info(String.format("%s already been used generating another id for the %s time", generatedId, counter));
			generatedId = IdGenerationUtility.generateId(predicate);
		}
		LOGGER.info(String.format("%s has been generated", generatedId));
		return generatedId;
	}
}
