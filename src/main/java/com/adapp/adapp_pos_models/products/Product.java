package com.adapp.adapp_pos_models.products;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "products")
public class Product {

	@Id
	private String id;
	private String name;
	private BigDecimal price;
	private String description;
	private String productCode;
	private ProductType productType;
	private int numberOfPax;
	private ProductCategory productCategory;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public int getNumberOfPax() {
		return numberOfPax;
	}

	public void setNumberOfPax(int numberOfPax) {
		this.numberOfPax = numberOfPax;
	}

	public ProductCategory getProductCategory() { return productCategory; }

	public void setProductCategory(ProductCategory productCategory) { this.productCategory = productCategory; }

	public enum ProductType {
		MAIN("Main"),
		ADD_ON("Add on");

		ProductType(String type) {
			this.type = type;
		}

		private String type;

		public String getType() {
			return type;
		}
	}

	public enum ProductCategory {
		UNLIMITED("Unlimited"),
		LIMITED("Limited");

		ProductCategory(String category) { this.category = category; }

		private String category;

		public String getCategory() { return category; }
	}

}
