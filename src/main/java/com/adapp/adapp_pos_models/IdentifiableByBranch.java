package com.adapp.adapp_pos_models;

public interface IdentifiableByBranch {
	
	void setBranchId(String branchId);
	String getBranchId();
}
