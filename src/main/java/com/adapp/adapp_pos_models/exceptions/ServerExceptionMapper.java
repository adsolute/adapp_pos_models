package com.adapp.adapp_pos_models.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * Maps {@link ServerException} to an internal server error status
 */
@Provider
public class ServerExceptionMapper implements ExceptionMapper<ServerException> {

	public Response toResponse(ServerException exception) {
		return Response.status( Status.INTERNAL_SERVER_ERROR).entity(exception.getMessage())
				.type(MediaType.APPLICATION_JSON ).build();
	}

}
