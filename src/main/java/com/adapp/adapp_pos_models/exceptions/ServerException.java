package com.adapp.adapp_pos_models.exceptions;

/**
 * Represents a generic server processing exception
 *
 */
public class ServerException extends BaseException{

	private static final long serialVersionUID = 1L;

	public ServerException() {
		super();
	}

	public ServerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServerException(String message) {
		super(message);
	}

	public ServerException(Throwable cause) {
		super(cause);
	}

}
