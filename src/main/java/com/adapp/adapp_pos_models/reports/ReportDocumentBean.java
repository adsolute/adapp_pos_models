package com.adapp.adapp_pos_models.reports;

public interface ReportDocumentBean {
	String getTemplateFilePath();
}
