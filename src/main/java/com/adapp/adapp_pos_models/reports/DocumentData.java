package com.adapp.adapp_pos_models.reports;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "document_data")
public class DocumentData implements ReportDocumentBean{
	@Id
	private String id;
	private String documentDataId;
	private String templateFilePath;
	@Field
	@Indexed(name = "expiry_date", expireAfter = "10s", direction = IndexDirection.ASCENDING)
	private DateTime requestDate;

	public DocumentData(String templateFilePath) {
		this.templateFilePath = templateFilePath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDocumentDataId() {
		return documentDataId;
	}

	public void setDocumentDataId(String documentDataId) {
		this.documentDataId = documentDataId;
	}

	public String getTemplateFilePath() {
		return templateFilePath;
	}

	public DateTime getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(DateTime requestDate) {
		this.requestDate = requestDate;
	}
}
