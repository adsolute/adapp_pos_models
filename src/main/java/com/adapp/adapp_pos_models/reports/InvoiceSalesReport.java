package com.adapp.adapp_pos_models.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InvoiceSalesReport {
	private BigDecimal totalSales;
	private Double totalOrders;
	private List<TotalInvoiceByHour> totalInvoiceByHours = new ArrayList<>();

	public BigDecimal getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

	public Double getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(Double totalOrders) {
		this.totalOrders = totalOrders;
	}

	public List<TotalInvoiceByHour> getTotalInvoiceByHours() {
		return totalInvoiceByHours;
	}

	public void setTotalInvoiceByHours(List<TotalInvoiceByHour> totalInvoiceByHours) {
		this.totalInvoiceByHours = totalInvoiceByHours;
	}

	public static class TotalInvoiceByHour {
		private int hour;
		private double totalInvoice;

		public int getHour() {
			return hour;
		}

		public void setHour(int hour) {
			this.hour = hour;
		}

		public double getTotalInvoice() {
			return totalInvoice;
		}

		public void setTotalInvoice(double totalInvoice) {
			this.totalInvoice = totalInvoice;
		}
	}
}
