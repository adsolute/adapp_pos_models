package com.adapp.adapp_pos_models.reports;

import com.adapp.adapp_pos_models.invoices.Invoice;

import java.util.List;

public class ReportRequest {
	private Invoice invoice;
	private List<Invoice> invoicesForDailyReport;
	private List<DailySalesReportBean.OrderedProduct> orderedProducts;

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public List<Invoice> getInvoicesForDailyReport() {
		return invoicesForDailyReport;
	}

	public void setInvoicesForDailyReport(List<Invoice> invoicesForDailyReport) {
		this.invoicesForDailyReport = invoicesForDailyReport;
	}

	public List<DailySalesReportBean.OrderedProduct> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(List<DailySalesReportBean.OrderedProduct> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}
}
