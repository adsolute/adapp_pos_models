package com.adapp.adapp_pos_models.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DailySalesReportBean extends DocumentData {
	private List<InvoiceDailySalesBean> invoiceDailySalesBeans;
	private BigDecimal totalCash = BigDecimal.ZERO;
	private BigDecimal totalGCash = BigDecimal.ZERO;
	private BigDecimal totalBankTransfer = BigDecimal.ZERO;
	private BigDecimal totalFoodPanda = BigDecimal.ZERO;
	private BigDecimal totalPettyCash = BigDecimal.ZERO;
	private BigDecimal totalLalamove = BigDecimal.ZERO;
	private BigDecimal totalGrab = BigDecimal.ZERO;
	private BigDecimal totalMetroDeal = BigDecimal.ZERO;
	private BigDecimal totalOther = BigDecimal.ZERO;
	private BigDecimal totalBalance = BigDecimal.ZERO;
	private BigDecimal totalSales = BigDecimal.ZERO;
	private BigDecimal totalDiscounts = BigDecimal.ZERO;
	private BigDecimal originalTotal = BigDecimal.ZERO;
	private List<OrderedProduct> orderedProducts = new ArrayList();


	public DailySalesReportBean (String templateFilePath) {
		super(templateFilePath);
	}

	public List<InvoiceDailySalesBean> getInvoiceDailySalesBeans() {
		return invoiceDailySalesBeans;
	}

	public void setInvoiceDailySalesBeans(List<InvoiceDailySalesBean> invoiceDailySalesBeans) {
		this.invoiceDailySalesBeans = invoiceDailySalesBeans;
	}

	public BigDecimal getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(BigDecimal totalCash) {
		this.totalCash = totalCash;
	}

	public BigDecimal getTotalGCash() {
		return totalGCash;
	}

	public void setTotalGCash(BigDecimal totalGCash) {
		this.totalGCash = totalGCash;
	}

	public BigDecimal getTotalBankTransfer() {
		return totalBankTransfer;
	}

	public void setTotalBankTransfer(BigDecimal totalBankTransfer) {
		this.totalBankTransfer = totalBankTransfer;
	}

	public BigDecimal getTotalFoodPanda() {
		return totalFoodPanda;
	}

	public void setTotalFoodPanda(BigDecimal totalFoodPanda) {
		this.totalFoodPanda = totalFoodPanda;
	}

	public BigDecimal getTotalPettyCash() {
		return totalPettyCash;
	}

	public void setTotalPettyCash(BigDecimal totalPettyCash) {
		this.totalPettyCash = totalPettyCash;
	}

	public BigDecimal getTotalLalamove() {
		return totalLalamove;
	}

	public void setTotalLalamove(BigDecimal totalLalamove) {
		this.totalLalamove = totalLalamove;
	}

	public BigDecimal getTotalGrab() {
		return totalGrab;
	}

	public void setTotalGrab(BigDecimal totalGrab) {
		this.totalGrab = totalGrab;
	}

	public BigDecimal getTotalMetroDeal() {
		return totalMetroDeal;
	}

	public void setTotalMetroDeal(BigDecimal totalMetroDeal) {
		this.totalMetroDeal = totalMetroDeal;
	}

	public BigDecimal getTotalOther() {
		return totalOther;
	}

	public void setTotalOther(BigDecimal totalOther) {
		this.totalOther = totalOther;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

	public BigDecimal getTotalDiscounts() {
		return totalDiscounts;
	}

	public void setTotalDiscounts(BigDecimal totalDiscounts) {
		this.totalDiscounts = totalDiscounts;
	}

	public BigDecimal getOriginalTotal() {
		return originalTotal;
	}

	public void setOriginalTotal(BigDecimal originalTotal) {
		this.originalTotal = originalTotal;
	}

	public List<OrderedProduct> getOrderedProducts() {
		return orderedProducts;
	}

	public void setOrderedProducts(List<OrderedProduct> orderedProducts) {
		this.orderedProducts = orderedProducts;
	}

	public static class InvoiceDailySalesBean {
		private String invoiceDate;
		private String rider;
		private String customer;
		private String invoiceNumber;
		private String paymentMethod;
		private String originalDue;
		private String discount;
		private String amountDue;
		private String balance;
		private String location;
		private String remarks;
		private String deliveryCode;
		private String incentive;
		private String numberOfPax;

		public String getInvoiceDate() {
			return invoiceDate;
		}

		public void setInvoiceDate(String invoiceDate) {
			this.invoiceDate = invoiceDate;
		}

		public String getRider() {
			return rider;
		}

		public void setRider(String rider) {
			this.rider = rider;
		}

		public String getCustomer() {
			return customer;
		}

		public void setCustomer(String customer) {
			this.customer = customer;
		}

		public String getInvoiceNumber() {
			return invoiceNumber;
		}

		public void setInvoiceNumber(String invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}

		public String getPaymentMethod() {
			return paymentMethod;
		}

		public void setPaymentMethod(String paymentMethod) {
			this.paymentMethod = paymentMethod;
		}

		public String getOriginalDue() {
			return originalDue;
		}

		public void setOriginalDue(String originalDue) {
			this.originalDue = originalDue;
		}

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public String getAmountDue() {
			return amountDue;
		}

		public void setAmountDue(String amountDue) {
			this.amountDue = amountDue;
		}

		public String getBalance() {
			return balance;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		public String getDeliveryCode() {
			return deliveryCode;
		}

		public void setDeliveryCode(String deliveryCode) {
			this.deliveryCode = deliveryCode;
		}

		public String getIncentive() {
			return incentive;
		}

		public void setIncentive(String incentive) {
			this.incentive = incentive;
		}

		public String getNumberOfPax() {
			return numberOfPax;
		}

		public void setNumberOfPax(String numberOfPax) {
			this.numberOfPax = numberOfPax;
		}
	}

	public static class OrderedProduct {
		String id;
		String code;
		String totalOrders;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getTotalOrders() {
			return totalOrders;
		}

		public void setTotalOrders(String totalOrders) {
			this.totalOrders = totalOrders;
		}
	}
}
