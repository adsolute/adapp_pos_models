package com.adapp.adapp_pos_models.audit;

import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.utils.FlatMapUtil;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.adapp.security.models.Permission;
import com.adapp.security.models.UserPrincipal;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.adapp.adapp_pos_models.enums.Resource.ROLES;

public class Audit {

    @Id
    private String id;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime auditDate;
    private UserPrincipal userPrincipal;
    private Resource resource;
    private AuditMessage.AuditAction auditAction;
    private String auditedId;
    private String forAccountId;
    private String identifierName;
    private String identifier;
    private Map<String, Object> onlyOnLeft;
    private Map<String, Object> onlyOnRight;
    private List<DifferingProperty> differingProperties;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(DateTime auditDate) {
        this.auditDate = auditDate;
    }

    public UserPrincipal getUserPrincipal() {
        return userPrincipal;
    }

    public void setUserPrincipal(UserPrincipal userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public AuditMessage.AuditAction getAuditAction() {
        return auditAction;
    }

    public void setAuditAction(AuditMessage.AuditAction auditAction) {
        this.auditAction = auditAction;
    }

    public String getAuditedId() {
        return auditedId;
    }

    public void setAuditedId(String auditedId) {
        this.auditedId = auditedId;
    }

    public String getForAccountId() {
        return forAccountId;
    }

    public void setForAccountId(String forAccountId) {
        this.forAccountId = forAccountId;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Map<String, Object> getOnlyOnLeft() {
        return onlyOnLeft;
    }

    public void setOnlyOnLeft(Map<String, Object> onlyOnLeft) {
        this.onlyOnLeft = onlyOnLeft;
    }

    public Map<String, Object> getOnlyOnRight() {
        return onlyOnRight;
    }

    public void setOnlyOnRight(Map<String, Object> onlyOnRight) {
        this.onlyOnRight = onlyOnRight;
    }

    public List<DifferingProperty> getDifferingProperties() {
        return differingProperties;
    }

    public void setDifferingProperties(List<DifferingProperty> differingProperties) {
        this.differingProperties = differingProperties;
    }

    private static class DifferingProperty {
        private String propertyName;
        private Object left;
        private Object right;

        public DifferingProperty(String propertyName, Object left, Object right) {
            this.propertyName = propertyName;
            this.left = left;
            this.right = right;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public Object getLeft() {
            return left;
        }

        public void setLeft(Object left) {
            this.left = left;
        }

        public Object getRight() {
            return right;
        }

        public void setRight(Object right) {
            this.right = right;
        }
    }

    public static class Builder<T> {

        public static final String PERMISSIONS = "permissions";
        private AuditMessage<T> auditMessage;

        public Builder<T> from(AuditMessage<T> auditMessage) {
            this.auditMessage = auditMessage;
            return this;
        }

        public Audit build() {
            Audit audit = new Audit();
            audit.setAuditDate(new DateTime());
            audit.setUserPrincipal(auditMessage.getUserPrincipal());
            audit.setResource(auditMessage.getResource());
            audit.setAuditAction(auditMessage.getAuditAction());
            audit.setAuditedId(auditMessage.getAuditedId());
            audit.setIdentifierName(auditMessage.getIdentifierName());
            audit.setIdentifier(auditMessage.getIdentifier());
            audit.setForAccountId(auditMessage.getForAccountId());
            if(auditMessage.getBefore() != null && auditMessage.getAfter() != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                TypeReference<HashMap<String, Object>> type =
                        new TypeReference<HashMap<String, Object>>() {};
                Map<String, Object> beforeMap = objectMapper.convertValue(auditMessage.getBefore(), type);
                Map<String, Object> afterMap = objectMapper.convertValue(auditMessage.getAfter(), type);
                if(ROLES == auditMessage.getResource()) {
                    flattenPermissions(beforeMap, afterMap);
                }
                Map<String, Object> beforeFlatMap = FlatMapUtil
                        .flatten(beforeMap);
                Map<String, Object> afterFlatMap = FlatMapUtil
                        .flatten(afterMap);
                MapDifference<String, Object> mapDifference = Maps.difference(beforeFlatMap, afterFlatMap);
                audit.setOnlyOnLeft(mapDifference.entriesOnlyOnLeft());
                audit.setOnlyOnRight(mapDifference.entriesOnlyOnRight());
                List<DifferingProperty> differingProperties = mapDifference.entriesDiffering()
                        .entrySet()
                        .stream()
                        .map(entry -> new DifferingProperty(entry.getKey(), entry.getValue().leftValue(), entry.getValue().rightValue()))
                        .collect(Collectors.toList());
                audit.setDifferingProperties(differingProperties);
            }
              return audit;
        }

        private void flattenPermissions(Map<String, Object> beforeMap, Map<String, Object> afterMap) {
            ObjectMapper objectMapper = new ObjectMapper();
            TypeReference<List<Permission<Resource>>> listPermissionTypeReference =
                    new TypeReference<List<Permission<Resource>>>() {};

            List<Permission<Resource>> permissionsBefore = objectMapper.convertValue(beforeMap.get(PERMISSIONS), listPermissionTypeReference);
            List<Permission<Resource>> permissionsAfter = objectMapper.convertValue(afterMap.get(PERMISSIONS), listPermissionTypeReference);

            String permissionsStringBefore = permissionsBefore.stream().map(permission -> String.format("%s:%s", permission.getResource(), permission.getAction()))
                    .collect(Collectors.toList()).stream().sorted().collect(Collectors.joining("\n"));
            String permissionsStringAfter = permissionsAfter.stream().map(permission -> String.format("%s:%s", permission.getResource(), permission.getAction()))
                    .collect(Collectors.toList()).stream().sorted().collect(Collectors.joining("\n"));
            
            beforeMap.put(PERMISSIONS, permissionsStringBefore);
            afterMap.put(PERMISSIONS, permissionsStringAfter);
        }
    }
}
