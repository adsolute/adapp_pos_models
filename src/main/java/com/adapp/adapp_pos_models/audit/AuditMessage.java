package com.adapp.adapp_pos_models.audit;

import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.security.models.UserPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class AuditMessage<T> {

    private UserPrincipal userPrincipal;
    private Resource resource;
    private AuditAction auditAction;
    private String auditedId;
    private String forAccountId;
    private String identifierName;
    private String identifier;
    private T before;
    private T after;

    private AuditMessage() {

    }

    public UserPrincipal getUserPrincipal() {
        return userPrincipal;
    }

    public void setUserPrincipal(UserPrincipal userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public AuditAction getAuditAction() {
        return auditAction;
    }

    public void setAuditAction(AuditAction auditAction) {
        this.auditAction = auditAction;
    }

    public String getAuditedId() {
        return auditedId;
    }

    public void setAuditedId(String auditedId) {
        this.auditedId = auditedId;
    }

    public String getForAccountId() {
        return forAccountId;
    }

    public void setForAccountId(String forAccountId) {
        this.forAccountId = forAccountId;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public T getBefore() {
        return before;
    }

    public void setBefore(T before) {
        this.before = before;
    }

    public T getAfter() {
        return after;
    }

    public void setAfter(T after) {
        this.after = after;
    }

    public static class Builder<T> {
        AuditMessage<T> auditMessage = new AuditMessage<>();

        public Builder<T> forResource(Resource resource) {
            this.auditMessage.setResource(resource);
            return this;
        }

        public Builder<T> withAction(AuditAction auditAction) {
            this.auditMessage.setAuditAction(auditAction);
            return this;
        }

        public Builder<T> beforeChange(T before) {
            this.auditMessage.setBefore(before);
            return this;
        }

        public Builder<T> afterChange(T after) {
            this.auditMessage.setAfter(after);
            return this;
        }

        public Builder<T> withAuditedId(String auditedId) {
            this.auditMessage.setAuditedId(auditedId);
            return this;
        }

        public Builder<T> forAccountId(String accountId) {
            this.auditMessage.setForAccountId(accountId);
            return this;
        }

        public Builder<T> withIdentifier(String identifierName, String identifier) {
            this.auditMessage.setIdentifierName(identifierName);
            this.auditMessage.setIdentifier(identifier);
            return this;
        }

        public AuditMessage<T> build() {
            Subject subject = SecurityUtils.getSubject();

            auditMessage.setUserPrincipal((UserPrincipal) subject.getPrincipal());

            return auditMessage;
        }
    }

    public enum AuditAction {
        CREATE,
        READ,
        UPDATE,
        DELETE
    }
}
