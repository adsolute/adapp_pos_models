package com.adapp.adapp_pos_models.audit;

import com.adapp.adapp_pos_models.enums.Resource;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Deserializer;
import com.adapp.adapp_pos_models.utils.UTC8Iso8601Serializer;
import com.adapp.security.models.UserPrincipal;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

public class AuditDTO {

    private String id;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime auditDate;
    private String username;
    private Resource resource;
    private AuditMessage.AuditAction auditAction;
    private String auditedId;
    private String forAccountId;
    private String identifierName;
    private String identifier;

    public AuditDTO() {
    }

    public AuditDTO (Audit audit) {
        this.id = audit.getId();
        this.auditDate = audit.getAuditDate();
        this.username = audit.getUserPrincipal().getUsername();
        this.resource = audit.getResource();
        this.auditAction = audit.getAuditAction();
        this.auditedId = audit.getAuditedId();
        this.forAccountId = audit.getForAccountId();
        this.identifierName = audit.getIdentifierName();
        this.identifier = audit.getIdentifier();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(DateTime auditDate) {
        this.auditDate = auditDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public AuditMessage.AuditAction getAuditAction() {
        return auditAction;
    }

    public void setAuditAction(AuditMessage.AuditAction auditAction) {
        this.auditAction = auditAction;
    }

    public String getAuditedId() {
        return auditedId;
    }

    public void setAuditedId(String auditedId) {
        this.auditedId = auditedId;
    }

    public String getForAccountId() {
        return forAccountId;
    }

    public void setForAccountId(String forAccountId) {
        this.forAccountId = forAccountId;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
