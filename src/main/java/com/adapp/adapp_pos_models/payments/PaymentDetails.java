package com.adapp.adapp_pos_models.payments;

import com.adapp.security.utils.UTC8Iso8601Deserializer;
import com.adapp.security.utils.UTC8Iso8601Serializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public class PaymentDetails {
    private BigDecimal amount;
    private String note;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime paymentDate;
    @JsonSerialize(using = UTC8Iso8601Serializer.class)
    @JsonDeserialize(using = UTC8Iso8601Deserializer.class)
    private DateTime updateDate;
    private PaymentMethod paymentMethod;
    private PaymentStatus paymentStatus;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(DateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public enum PaymentMethod {
        CASH,
        GCASH,
        BANK_TRANSFER,
        FOOD_PANDA,
        PETTY_CASH,
        LALAMOVE,
        GRAB,
        METRO_DEAL,
        OTHER
    }

    public enum PaymentStatus {
        UNPAID,
        PARTIAL,
        PAID
    }
}
